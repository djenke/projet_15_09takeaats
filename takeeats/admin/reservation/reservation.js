$.ajax({
    url: "../../php/admin/reservation.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            res.reservations.forEach(reservation => {
                console.log(reservation);
                const tr = $("<tr></tr>"); // Je crée une nouvelle ligne

                const name_reservation = $("<td></td>").text(reservation.name_reservation); // Je crée une case pour le nom
                const number_of_People = $("<td></td>").text(reservation.number_of_People); // Je crée une country
                const date_reservation = $("<td></td>").text(reservation.date_reservation); // Je crée une case pour le phone
                const id_users = $("<tr></tr>").text(reservation.id_users); //Je crée une case pour numéro de rue 
                const updatebtn = $("<button></button>");
                updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute un texte au lien

                // J'ajoute un écouteur d'événement clic sur le bouton modifier
                updatebtn.click(() => {
                    // Je modifie le titre de la box du fomulaire
                    $(".box h1").text("Modifier le restaurant");

                    // J'affecte à la variable définit en haut du fichier la valeur de l'id de l'article sur lequel on a cliqué
                    reservation_id = reservation.id;

                    $(".box").addClass("open"); // J'affiche mon formulaire
                    $("#overlay").addClass("open"); // J'affiche mon overlay
                });
                const deletebtn = $("<button></button>");
                deletebtn.html("<i class='fa fa-trash aria-hidden='true></i>");

                tr.addClass("reservation")
                tr.append(name_reservation, number_of_People, date_reservation, id_users, updatebtn, deletebtn);
                // J'ajoute toutes mes cases dans ma ligne
                $("#reservation").append(tr);

            });


        }


    }
});