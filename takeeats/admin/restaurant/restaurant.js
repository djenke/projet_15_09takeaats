//? Si mon utilisateur n'est pas admin je redirige vers l'accueil
//if (user.admin == 0) window.location.replace("../../restaurant/restaurant.html");

$.ajax({
    url: "../../php/admin/restaurant.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            res.restaurants.forEach(restaurant => {
                console.log(restaurant);
                const tr = $("<tr></tr>"); // Je crée une nouvelle ligne



                const name = $("<td></td>").text(restaurant.name); // Je crée une case pour le nom
                const country = $("<td></td>").text(restaurant.country); // Je crée une country
                const phone = $("<td></td>").text(restaurant.phone); // Je crée une case pour le phone
                const street_number = $("<tr></tr>").text(restaurant.street_number); //Je crée une case pour numéro de rue 
                const street_name = $("<td></td>").text(restaurant.street_name); // Je crée une case pour le nom de rue
                const postal_code = $("<td></td>").text(restaurant.postal_code); // Je crée une case pour le code postale
                const description = $("<td></td>").text(restaurant.description); // Je crée une case pour la description
                const image = $("<td></td>").text(restaurant.image); // Je crée une case pour l'image
                const number_of_place = $("<td></td>") //.text(restaurant.number_of_place);  Je crée une case pour le nombre de places disponibles
                const updatebtn = $("<button></button>");
                updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute un texte au lien

                // J'ajoute un écouteur d'événement clic sur le bouton modifier
                updatebtn.click(() => {
                    // Je modifie le titre de la box du fomulaire
                    $(".box h1").text("Modifier le restaurant");

                    // J'affecte à la variable définit en haut du fichier la valeur de l'id de l'article sur lequel on a cliqué
                    restaurant_id = restaurant.id;

                    $(".box").addClass("open"); // J'affiche mon formulaire
                    $("#overlay").addClass("open"); // J'affiche mon overlay
                });
                const deletebtn = $("<button></button>");
                deletebtn.html("<i class='fa fa-trash aria-hidden='true></i>");


                tr.addClass("resto")
                tr.append(name, description, phone, country, postal_code, street_number, street_name, image, number_of_place, updatebtn, deletebtn);
                // J'ajoute toutes mes cases dans ma ligne
                $("#resto").append(tr);

            });


        }


    }
});



$.ajax({
    url: "../../php/admin/restaurant.php", // URL cible
    type: "POST", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { choice: "insert" }

});

function insertRestaurant(name, desc, picture, cat_ids) {
    // Je crée une nouvelle instance de FormData afin d'ajouter chaque de mes clés
    const fd = new FormData();
    fd.append("choice", "insert");
    fd.append("name", name);
    fd.append("desc", desc);
    fd.append("cat_ids", JSON.stringify(cat_ids));
    fd.append("picture", picture);

    $.ajax({
        url: "../php/article.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        cache: false,
        data: fd,
        success: (res) => {
            if (res.success) {
                // J'appelle la fonction addArticle afin de créer un article par rapport aux données du formulaire
                addArticle({
                    id: res.id,
                    name,
                    description: desc,
                    author: user.firstname + " " + user.lastname,
                    image: res.image,
                    categories: res.categories
                });

                $(".box").removeClass("open"); // Je cache mon formulaire
                $("#overlay").removeClass("open"); // Je cache mon overlay
            } else alert(res.error);
        }
    });
}

console.log('$_SESSION');

function addRestaurant(restaurant) {
    const ctn = $("<article></article>"); // Je crée un élement article
    ctn.addClass("art_box"); // J'ajoute à mon élément article la classe 'art_box' 

    const street_name = $("<h2></h2>").text(restaurant.name); // Je crée un élément h2 et je lui ajoute le texte correspondant au nom de mon article

    const desc = $("<p></p>").text(restaurant.description); // Je crée un élément p et je lui ajoute le texte correspondant à la description de mon article
    desc.addClass("desc"); // J'ajoute la classe desc au paragraphe de la description

    const author = $("<p></p>").text(restaurant.author); // Je crée un élément p et je lui ajoute le texte correspondant à l'auteur de mon article

    const created_at = $("<small></small>"); // Je crée un élément small

    let date; // Je crée une variable date qui va contenir la date de création de l'article

    //? Si l'article à une date de création je l'affecte à la variable date
    if (article.created_at) date = new Date(article.created_at);
    //? Sinon je crée une date
    else date = new Date();

    let day = date.getDate(); // J'attribue à une variable la valeur du jour de la date que j'ai créée.
    if (day < 10) day = '0' + day; // Si le jour est inférieur à 10 je rajoute un 0 devant

    let month = date.getMonth() + 1; // J'attribue à une variable la valeur du mois de la date que j'ai créée. J'ajoute +1 car le mois commence à 0
    if (month < 10) month = '0' + month; // Si le mois est inférieur à 10 je rajoute un 0 devant

    let hours = date.getHours(); // J'attribue à une variable la valeur des heures de la date que j'ai créée.
    if (hours < 10) hours = '0' + hours; // Si l'heure est inférieur à 10 je rajoute un 0 devant

    let min = date.getMinutes(); // J'attribue à une variable la valeur ddes minutes de la date que j'ai créée.
    if (min < 10) min = '0' + min; // Si les minutes sont inférieurs à 10 je rajoute un 0 devant

    // J'attribue à l'element small le texte correspondant à la date de création de mon article au format jj/mm/yyyy hh:mm
    created_at.text(day + "/" + month + "/" + date.getFullYear() + " " + hours + "h" + min);

    ctn.append(title, desc, author, created_at); // J'ajoute les éléments title, desc, author et date dans mon article

    //? Si j'ai une date de création de l'article alors c'est un article existant je l'ajoute dans la liste
    if (article.created_at) $("#art_ctn").append(ctn);
    //? Sinon c'est un nouvel article je l'ajoute au début de la liste
    else $("#art_ctn").prepend(ctn);
}