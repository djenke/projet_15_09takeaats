$.ajax({
    url: "../../php/admin/user.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            res.users.forEach(user => {
                console.log(user);
                const tr = $("<tr></tr>"); // Je crée une nouvelle ligne



                const id_users = $("<td></td>").text(user.id_users); // Je crée une case pour le nom
                const firstname = $("<td></td>").text(user.firstname); // Je crée une country
                const lastname = $("<td></td>").text(user.lastname); // Je crée une case pour le phone
                const email = $("<tr></tr>").text(user.email); //Je crée une case pour numéro de rue 
                const updatebtn = $("<button></button>");
                updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute un texte au lien
                // J'ajoute un écouteur d'événement clic sur le bouton modifier
                updatebtn.click(() => {
                    // Je modifie le titre de la box du fomulaire
                    $(".box h1").text("Modifier le user");
                    // J'affecte à la variable définit en haut du fichier la valeur de l'id de l'article sur lequel on a cliqué
                    user_id = user.id;
                    $(".box").addClass("open"); // J'affiche mon formulaire
                    $("#overlay").addClass("open"); // J'affiche mon overlay
                });
                const deletebtn = $("<button></button>");
                deletebtn.html("<i class='fa fa-trash aria-hidden='true></i>");
                tr.addClass("user")
                tr.append(id_users, lastname, firstname, email, updatebtn, deletebtn);
                // J'ajoute toutes mes cases dans ma ligne
                $("#user").append(tr);

            });


        }


    }
});