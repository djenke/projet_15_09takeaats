$(document).ready(() => {
    $.ajax({
        url: "../../php/comment.php",
        type: "GET",
        dataType: "json",
        data: {
            choice: "selector"
        },
        success: (res) => {


            if (res.success) {

                const restaurants = res.comments;
                afficherOptions(restaurants);
            }
        }
    });
});

function insertComment(fd) {
    fd.append("choice", "insert");


    $.ajax({
        url: "../../php/comment.php",
        type: "POST",
        dataType: "json",
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            if (res.success) {
                window.location.replace("../comment.html");
            }
        }
    });
}











function afficherOptions(restaurants) {
    const select = $("<select></select>");
    select.append("<option value=''>Tous les restaurants</option>");

    restaurants.forEach(restaurant => {
        const option = $("<option></option>")
            .attr("value", restaurant.id_restaurant)
            .text(restaurant.name);

        select.append(option);
    });

    $("form").append(select);

    $("form").submit(event => {
        event.preventDefault();

        const fd = new FormData();
        const selectedRestaurantId = $(select).val(); // Récupère l'ID du restaurant sélectionné
        fd.append("id_restaurant", selectedRestaurantId);
        fd.append("content", $("#content").val());

        insertComment(fd);
    });
}