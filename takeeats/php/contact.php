<?php
session_start();
require_once("utils/db_connect.php");
require("utils/mailer.php");

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
    die;
}

if (!isset($_POST["lastname"], $_POST["email"], $_POST["content"])) {
    echo json_encode(["success" => false, "error" => "Données manquantes"]);
    die;
}

if (
    empty(trim($_POST["lastname"])) ||

    empty(trim($_POST["email"])) ||
    empty(trim($_POST["content"]))
) {
    echo json_encode(["success" => false, "error" => "Données vides"]);
    die;
}

$regex = "/^[a-zA-Z0-9-+._]+@[a-zA-Z0-9-]{2,}\.[a-zA-Z]{2,}$/";
if (!preg_match($regex, $_POST["email"])) {
    echo json_encode(["success" => false, "error" => "Email au mauvais format"]);
    die;
}


$reqCheckEmail = $db->prepare("SELECT session_id FROM sessions WHERE email = ?");
$reqCheckEmail->execute(array($_POST["email"]));
$resultCheckEmail = $reqCheckEmail->fetch(PDO::FETCH_ASSOC);

if ($resultCheckEmail) {
    $sessions_id = $resultCheckEmail['session_id'];
} else {
    $sessions_id = null;
}

if (!isset($_SESSION['admin_id'])) {
    echo json_encode(["success" => false, "error" => "L'administrateur n'est pas connecté"]);
    die;
}

$admin_id = $_SESSION['admin_id'];

$req = $db->prepare("INSERT INTO users (lastname, phone, email, content, admin_id, sessions_id) VALUES (?, ?, ?, ?, ?, ?)");
$req->execute(array($_POST["lastname"], $_POST["phone"], $_POST["email"], $_POST["content"], $admin_id, $sessions_id));

if ($req->rowCount()) {
    echo json_encode(["success" => true]);
} else {
    echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion des données"]);
}

mailer($_POST["email"], "Bienvenue {$_POST["lastname"]}", "Merci pour votre message. Nous vous répondrons dans les plus brefs délais.");
?>
