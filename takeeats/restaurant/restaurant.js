//? Si mon utilisateur n'est pas admin je redirige vers l'accueil
//if (user.admin == 0) window.location.replace("../../restaurant/restaurant.html");

/**
 * @desc Fait appel au php pour supprimer un restaurant
 * @param string id - Contient l'id de l'restaurant
 * @return void - Ne retourne rien
 */

$.ajax({
    url: "../php/restaurant.php",
    type: "GET",
    dataType: "json",
    data: {
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            const productContainer = $(".product-container"); // Sélectionnez le conteneur une seule fois

            res.restaurants.forEach(restaurant => {
                const productBox = $("<div>").addClass("product-box");
                const name = $("<h2>").text(restaurant.name);
                const postal_code = $("<p></p>").text(restaurant.postal_code)
                const street_number = $("<p></p>").text(restaurant.street_number)
                const street_name = $("<p></p>").text(restaurant.street_name)
                const description = $("<p></p>").text(restaurant.description)
                const reservationbtn = $("<button>").text("Réserver");
                // Au clic de la div "Ajouter un restaurant"
                reservationbtn.click(() => {
                    // Je redirige vers la page du formulaire
                    window.location.replace("manage_resto_user/manage_resto_user.html?id_restaurant=" + restaurant.id_restaurant);
                });
                const imagectn = $("<p></p>"); // Créez une case pour l'image

                productBox.append(imagectn, name, postal_code, street_name, street_number, description, reservationbtn);
                productContainer.append(productBox);
            });
        }



    }
});





console.log("================================");